package com.example.shlyk_l17_t1;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class DialogFragment extends Fragment implements OnClickListener {

    private RecyclerView recyclerView;
    private DataAdapter dataAdapter;
    private Button addButton;
    private List<HouseInfo> houses;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewList);
        addButton = view.findViewById(R.id.buttonAdd);
        initializedRecyclerView();
        initializedAddButton();
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        setupSwipeToRefresh();

        return view;
    }

    @Override
    public void onClick(final int position) {
        FragmentUtils.showDialog(getContext(),
                "you want to delete house?",
                "yes",
                "no",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        deleteHouse(position);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();
                    }
                });
    }

    private void initializedRecyclerView(){
        houses = ValuesClass.genericHousesList(3);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        dataAdapter = new DataAdapter(houses);
        dataAdapter.setDeleteBtnListener(this);
        recyclerView.setAdapter(dataAdapter);
    }

    private void initializedAddButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addHouse();
            }
        });
    }


    private void setupSwipeToRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                houses = ValuesClass.genericHousesList(5);
                dataAdapter.addHouse(houses);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void deleteHouse(int position){
        List<HouseInfo> newHouseList = new ArrayList<>(houses);
        newHouseList.remove(position);
        dataAdapter.addHouse(newHouseList);
        houses.clear();
        houses.addAll(newHouseList);
    }

    private void addHouse(){
        List<HouseInfo> newHouseList = new ArrayList<>(houses);
        newHouseList.add(ValuesClass.createRandomHouse());
        dataAdapter.addHouse(newHouseList);
        houses.clear();
        houses.addAll(newHouseList);
        recyclerView.smoothScrollToPosition(houses.size() - 1);
    }
}
