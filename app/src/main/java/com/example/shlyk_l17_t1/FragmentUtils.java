package com.example.shlyk_l17_t1;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class FragmentUtils {
    public static void showDialog(
            final Context context,
            String message,
            String positiveButtonText,
            String negativeButtonText,
            DialogInterface.OnClickListener onPositiveButtonClick,
            DialogInterface.OnClickListener onNegativeButtonClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(positiveButtonText, onPositiveButtonClick)
                .setNegativeButton(negativeButtonText, onNegativeButtonClick)
                .setCancelable(false)
                .show();
    }
}
