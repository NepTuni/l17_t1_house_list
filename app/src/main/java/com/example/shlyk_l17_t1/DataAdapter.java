package com.example.shlyk_l17_t1;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;


import androidx.annotation.NonNull;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;



public class DataAdapter extends RecyclerView.Adapter<AdapterHolder> {

    private List<HouseInfo> houses;

    private OnClickListener onDeleteButtonClick =null;


    public DataAdapter(List<HouseInfo> houses)
    {
        this.houses = houses;
    }


    @NonNull
    @Override
    public AdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_house, parent, false);
        return new AdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterHolder holder, final int position) {
        holder.houseImage.setImageResource(houses.get(position).getHouseImage());
        holder.description.setText(houses.get(position).getDescription());
        holder.title.setText(houses.get(position).getHouseTitle());
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteHouse(v, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount(){
        return houses.size();
    }

    public void setDeleteBtnListener(OnClickListener deleteBtnListener){
        this.onDeleteButtonClick = deleteBtnListener;
    }

    private void onDeleteHouse(View view, int position){
        if (houses.size() > 0){
            if (onDeleteButtonClick != null){
                onDeleteButtonClick.onClick(position);
            }
        } else {

            Toast.makeText(view.getContext(), "List is empty", Toast.LENGTH_SHORT).show();
        }
    }


    public void addHouse(List<HouseInfo> insertElement){
        MyDiffUtilCallback diffUtilCallback = new MyDiffUtilCallback(insertElement, houses);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffUtilCallback);
        houses.clear();
        houses.addAll(insertElement);
        diffResult.dispatchUpdatesTo( this);
    }

}
