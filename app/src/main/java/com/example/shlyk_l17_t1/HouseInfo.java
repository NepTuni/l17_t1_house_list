package com.example.shlyk_l17_t1;

public class HouseInfo  {

    private String houseTitle;
    private String description;
    private int houseImage;

    public HouseInfo(String houseTitle, String description, int houseImage){
        this.houseTitle = houseTitle;
        this.description = description;
        this.houseImage = houseImage;
    }

    public HouseInfo(HouseInfo house){
        this.houseTitle = house.getHouseTitle();
        this.description = house.getDescription();
        this.houseImage = house.getHouseImage();
    }

    public String getHouseTitle(){
        return this.houseTitle;
    }

    public void setHouseTitle(String houseTitle){
        this.houseTitle = houseTitle;
    }

    public String getDescription(){
        return this.description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public int getHouseImage(){
        return this.houseImage;
    }

    public void setHouseImage(int houseImage){
        this.houseImage = houseImage;
    }

}
