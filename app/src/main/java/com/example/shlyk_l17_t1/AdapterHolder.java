package com.example.shlyk_l17_t1;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class AdapterHolder extends RecyclerView.ViewHolder {

    ImageView houseImage;
    ImageButton deleteButton;
    TextView description;
    TextView title;


    public AdapterHolder(View view) {
        super(view);
        houseImage = view.findViewById(R.id.imageHouse);
        title = view.findViewById(R.id.houseTitle);
        description = view.findViewById(R.id.description);
        deleteButton = view.findViewById(R.id.imageButtonDelete);


    }
}
