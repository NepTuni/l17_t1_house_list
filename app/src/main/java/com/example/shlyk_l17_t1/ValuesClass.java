package com.example.shlyk_l17_t1;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ValuesClass {

    private static List<Integer> createImageHouse(){
        List<Integer> imageHouseList = new LinkedList<>();
        imageHouseList.add(R.drawable.ic_house_green);
        imageHouseList.add(R.drawable.ic_house_default);
        imageHouseList.add(R.drawable.ic_house_red);
        imageHouseList.add(R.drawable.ic_house_yellow);
        return imageHouseList;
    }

    private static  List<String> createTitleHouse(){
        List<String> titleHouseList = new LinkedList<>();
        titleHouseList.add("Earth");
        titleHouseList.add("Uranus");
        titleHouseList.add("Saturn");
        titleHouseList.add("Sun");
        titleHouseList.add("Moon");
        titleHouseList.add("Mercury");
        titleHouseList.add("Neptune");
        return titleHouseList;

    }

    private static List<String> createDescriptionHouse(){
        List<String> descriptionHouseList = new LinkedList<>();
        descriptionHouseList.add("2 rooms, 1 kitchen, monotonous interior");
        descriptionHouseList.add("3 rooms, 1 kitchen, monotonous interior");
        descriptionHouseList.add("4 rooms, 2 kitchen, multi-colored interior");
        descriptionHouseList.add("2 rooms, 1 kitchen, monotonous interior");
        descriptionHouseList.add("3 rooms, 1 kitchen, multi-colored interior");
        descriptionHouseList.add("4 rooms, 2 kitchen, monotonous interior");
        descriptionHouseList.add("2 rooms, 1 kitchen, monotonous interior");
        descriptionHouseList.add("3 rooms, 2 kitchen, multi-colored interior");
        descriptionHouseList.add("4 rooms, 2 kitchen, monotonous interior");
        descriptionHouseList.add("2 rooms, 1 kitchen, multi-colored interior");
        descriptionHouseList.add("3 rooms, 1 kitchen, monotonous interior");
        descriptionHouseList.add("4 rooms, 3 kitchen, multi-colored interior");
        return descriptionHouseList;


    }

    public static  List<HouseInfo> genericHousesList(int count){
        List<HouseInfo> houses = new LinkedList<>();
        List<Integer> genericImageHouseList = createImageHouse();
        List<String> genericTitleHouse = createTitleHouse();
        List<String> genericDescriptionHouse = createDescriptionHouse();
        Random randomValue = new Random();
        for(int i = 0; i < count; i++){
            int number  = randomValue.nextInt(genericTitleHouse.size());
            String titleHouse = genericTitleHouse.get(number);
            number  = randomValue.nextInt(genericDescriptionHouse.size());
            String descriptionHouse = genericDescriptionHouse.get(number);
            number  = randomValue.nextInt(genericImageHouseList.size());
            int imageHouse = genericImageHouseList.get(number);
            houses.add(new HouseInfo(titleHouse, descriptionHouse, imageHouse));
        }
        return houses;

        }

        public static HouseInfo createRandomHouse(){
            List<Integer> genericImageHouseList = createImageHouse();
            List<String> genericTitleHouse = createTitleHouse();
            List<String> genericDescriptionHouse = createDescriptionHouse();
            Random randomValue = new Random();
                int number  = randomValue.nextInt(genericTitleHouse.size());
                String titleHouse = genericTitleHouse.get(number);
                number  = randomValue.nextInt(genericDescriptionHouse.size());
                String descriptionHouse = genericDescriptionHouse.get(number);
                number  = randomValue.nextInt(genericImageHouseList.size());
                int imageHouse = genericImageHouseList.get(number);
                HouseInfo house = new HouseInfo(titleHouse, descriptionHouse, imageHouse);
            return house;
        }
}
